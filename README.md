# S2HAND DEPLOYMENTS

## Minikube

- Kong

  ```
  kubectl create -f https://bit.ly/k4k8s
  ```

- Minikube Tunnel

  ```
  minikube tunnel
  ```

- Update local DNS host

  ```
  kubectl get service kong-proxy -n kong
  ```

  - Get the `EXTERNAL_IP`
  - Enter DNS mapping to `/etc/hosts` or `C:\Windows\System32\drivers\etc\hosts`

    ```
    ${EXTERNAL_IP} s2hand.k8s.com
    ```

- Production Deployments

  ```
  kubectl apply -f s2hand-ingress.kong.yml
  ```
